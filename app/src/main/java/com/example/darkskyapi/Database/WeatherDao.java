package com.example.darkskyapi.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.darkskyapi.Model.Weather;

import java.util.List;

@Dao
public interface WeatherDao {
    @Query("SELECT * FROM weather")
    List<Weather> getAll();

    @Insert
    void insert(Weather weather);

    @Delete
    void delete(Weather weather);

    @Update
    void update(Weather weather);

    @Query("DELETE FROM weather")
    void deleteAll();

}
