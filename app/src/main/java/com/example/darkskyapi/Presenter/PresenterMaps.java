package com.example.darkskyapi.Presenter;

public class PresenterMaps implements IPresenterMaps {
    private int numberList;

    @Override
    public void setNumberList(int numberList) {

        this.numberList = numberList;
    }

    @Override
    public int getNumberList() {
        return numberList;
    }

    @Override
    public boolean checkIsNumeric(String text) {
        try {
            double d = Double.parseDouble(text);

        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}
