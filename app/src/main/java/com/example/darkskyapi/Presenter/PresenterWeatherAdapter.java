package com.example.darkskyapi.Presenter;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class PresenterWeatherAdapter implements IPresenterWeatherAdapter {
    public PresenterWeatherAdapter(){
    }

    @Override
    public String getTemperatureText(Double temperature) {
        if(temperature == null) return "";
        String temp = String.format("%.1f°C", temperature);
        return temp.replace(",",".");
    }

    @Override
    public String getHumidityText(Double humidity) {
        if(humidity == null) return "";
        return (String.format( "%d%%", (int)(humidity * 100)));
    }

    @Override
    public String getPressureText(Double pressure) {
        if(pressure == null) return "";
        String pressureText = new DecimalFormat("#").format(pressure);
        return (String.format("%s hPa", pressureText));
    }

    @Override
    public String getWindSpeedText(Double windSpeed) {
        if(windSpeed == null) return "";
        String windSpeedText = new DecimalFormat("#.#").format(windSpeed);
        return (String.format("%s km/h", windSpeedText.replace(",", ".")));
    }

    @Override
    public String getPrecipIntensityText(Double precipIntensity) {
        if(precipIntensity == null) return "";
        String precipIntensityText = new DecimalFormat("#.##").format(precipIntensity);
        return (String.format("%s mm", precipIntensityText.replace(",", ".")));
    }

    @Override
    public String getPrecipProbabilityText(Double precipProbability) {
        if(precipProbability == null) return "";
        return (String.format("%d%%", (int) (precipProbability * 100)));
    }

    @Override
    public String getCloudCoverText(Double cloudCover) {
        if(cloudCover == null) return "";
        return (String.format("%d%%", (int) (cloudCover * 100)));
    }

    @Override
    public String getSummaryText(String summary) {
        if(summary == null) return "";
        return summary;
    }

    @Override
    public String getDateText(Double date) {
        if(date == null) return "";
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        return formatter.format(date * 1000).toLowerCase();
    }
}
