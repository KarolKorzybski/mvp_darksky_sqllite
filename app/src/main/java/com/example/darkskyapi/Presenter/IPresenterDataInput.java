package com.example.darkskyapi.Presenter;


public interface IPresenterDataInput {
    boolean checkIsNumeric(String text);
    void setNumberList(int numberList);
    int getNumberList();
    CharSequence[] createArray();
    String getLangToDialog(int number);
    String getLangToIntent();
}
