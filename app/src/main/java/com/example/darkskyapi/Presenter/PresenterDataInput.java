package com.example.darkskyapi.Presenter;

import com.example.darkskyapi.R;
import com.example.darkskyapi.View.ViewDataInput;

import java.util.Arrays;
import java.util.List;

public class PresenterDataInput implements IPresenterDataInput {
    private ViewDataInput view;
    private List<String> lang;
    private List<String> shortcutLang;
    private int numberList;

    public PresenterDataInput(ViewDataInput view) {
        this.view = view;
        lang = Arrays.asList(view.getResources().getStringArray(R.array.langArray));
        shortcutLang = Arrays.asList(view.getResources().getStringArray(R.array.shortcutLangArray));
    }

    @Override
    public boolean checkIsNumeric(String text) {
        try {
            double d = Double.parseDouble(text);

        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public CharSequence[] createArray() {
        CharSequence[] items = new CharSequence[lang.size()];
        for (int i = 0; i < lang.size(); i++) {
            items[i] = lang.get(i);
        }
        return items;
    }

    @Override
    public String getLangToDialog(int number) {
        setNumberList(number);
        return lang.get(number);
    }

    @Override
    public String getLangToIntent() {
        return shortcutLang.get(numberList);
    }

    @Override
    public void setNumberList(int numberList) {

        this.numberList = numberList;
    }

    @Override
    public int getNumberList() {
        return numberList;
    }

}
