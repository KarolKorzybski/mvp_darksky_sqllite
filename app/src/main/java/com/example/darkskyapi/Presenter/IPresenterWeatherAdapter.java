package com.example.darkskyapi.Presenter;

public interface IPresenterWeatherAdapter {
    String getTemperatureText(Double temperature);
    String getHumidityText(Double humidity);
    String getPressureText(Double pressure);
    String getWindSpeedText(Double windSpeed);
    String getPrecipIntensityText(Double precipIntensity);
    String getPrecipProbabilityText(Double precipProbability);
    String getCloudCoverText(Double cloudCover);
    String getSummaryText(String summary);
    String getDateText(Double date);
}
