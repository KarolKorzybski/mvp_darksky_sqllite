package com.example.darkskyapi.Presenter;
import com.example.darkskyapi.Model.Weather;
import com.example.darkskyapi.R;
import com.example.darkskyapi.Rest.RestSingletonWeather;
import com.example.darkskyapi.Rest.WeatherResponse;
import com.example.darkskyapi.View.ViewWeather;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterWeather implements IPresenterWeather {
    private ViewWeather view;
    private boolean isLoading = false;
    String latitude, longitude, language;
    private List<Weather> weathers;

    public PresenterWeather(ViewWeather view, String latitude, String longitude, String language, List<Weather> weathers) {
        this.view = view;
        this.weathers = weathers;
        this.latitude = latitude;
        this.longitude = longitude;
        this.language = language;
        loadWeather(latitude, longitude);
    }

    public PresenterWeather(ViewWeather view, List<Weather> weathers) {
        this.view = view;
        this.weathers = weathers;
        view.getWeatherWithDatabase();
    }

    @Override
    public void loadWeather(final String latitude, final String longitude) {
        if ((latitude == null || latitude.equals("")) || (longitude == null || longitude.equals(""))) {
            view.setVisible(false);
            view.showAlert("Info","Database is empty!\nPlease click pencil and select localisation!");
            return;
        }
        if (isLoading) {
            return;
        }
        if (language == null || language.equals("")) language = "en";
        isLoading = true;
        view.setVisible(true);

        String key = view.getString(R.string.key);
        Call<WeatherResponse> call = RestSingletonWeather.getInstance(view).restServiceWeather.getWeathers(key, latitude, longitude, language, "si");

        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                isLoading = false;
                WeatherResponse weatherResponse = response.body();
                if (response.isSuccessful() && weatherResponse != null && weatherResponse.weatherData != null) {
                    view.deleteDatabase();
                    weathers.clear();
                    weathers.addAll(weatherResponse.weatherData.data);
                    view.setVisible(false);
                    view.setWeather(weathers);
                    if (weatherResponse.timezone != null && !weatherResponse.timezone.equals("")) {
                        for (int i = 0; i < weathers.size(); i++) {
                            weathers.get(i).setTimezone(weatherResponse.timezone);
                            weathers.get(i).setLatitude(latitude);
                            weathers.get(i).setLongitude(longitude);
                            weathers.get(i).setLanguage(language);
                        }
                        view.setTimeZone(weatherResponse.timezone);
                        view.setVisibleBtnRefresh(true);
                    }
                } else if (!response.isSuccessful()) {
                    try {
                        view.showAlert("error", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                t.printStackTrace();
                isLoading = false;
                view.setVisible(true);
            }
        });
    }

    @Override
    public void loadWeatherWithDatabase(List<Weather> weathers) {
        if (isEmpty(weathers)) {
            loadWeather(latitude, longitude);
        } else {
            this.weathers.clear();
            this.weathers.addAll(weathers);
            view.setVisible(false);
            view.setWeather(weathers);
            if (weathers.get(0).timezone != null && !weathers.get(0).timezone.equals("")) {
                view.setTimeZone(weathers.get(0).timezone);
            }
            view.setVisibleBtnRefresh(true);
        }

    }

    @Override
    public boolean isEmpty(List<Weather> weathers) {
        return weathers.isEmpty();
    }

    @Override
    public void refreshDatabase() {
        if(weathers==null || weathers.isEmpty()) return;
        this.language = weathers.get(0).language;
        this.latitude = weathers.get(0).latitude;
        this.longitude = weathers.get(0).longitude;
        loadWeather(latitude,longitude);
    }

}
