package com.example.darkskyapi.Presenter;

public interface IPresenterMaps {

    boolean checkIsNumeric(String text);
    void setNumberList(int numberList);
    int getNumberList();
}
