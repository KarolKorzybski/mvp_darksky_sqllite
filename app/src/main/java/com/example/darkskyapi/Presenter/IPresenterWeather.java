package com.example.darkskyapi.Presenter;

import com.example.darkskyapi.Model.Weather;

import java.util.List;


public interface IPresenterWeather {
    void loadWeather(String latitude, String longitude);
    void loadWeatherWithDatabase(List<Weather> weathers);
    boolean isEmpty(List<Weather> weathers);
    void refreshDatabase();
}
