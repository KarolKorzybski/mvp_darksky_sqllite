package com.example.darkskyapi.Rest;

import com.example.darkskyapi.Model.WeatherData;
import com.google.gson.annotations.SerializedName;

public class WeatherResponse {
    @SerializedName("timezone")
    public String timezone;

    @SerializedName("daily")
    public WeatherData weatherData;
}
