package com.example.darkskyapi.Rest;

import android.content.Context;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RestSingletonWeather {

    private static RestSingletonWeather instance;
    public RestServiceWeather restServiceWeather;

    private final Context mContext;

    public static synchronized RestSingletonWeather getInstance(Context context) {
        if (instance == null) {
            instance = new RestSingletonWeather(context);
        }
        return instance;
    }

    private RestSingletonWeather(Context context) {
        mContext = context.getApplicationContext();


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofitWeather = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://api.darksky.net/forecast/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        restServiceWeather = retrofitWeather.create(RestServiceWeather.class);
    }
}
