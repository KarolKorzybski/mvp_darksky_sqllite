package com.example.darkskyapi.Rest;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestServiceWeather {

    @GET("{key}/{latitude},{longitude}")
    Call<WeatherResponse> getWeathers(
            @Path("key") String key,
            @Path("latitude") String latitude,
            @Path("longitude") String longitude,
            @Query("lang") String lang,
            @Query("units") String units
    );
}
