package com.example.darkskyapi.Model;

import com.example.darkskyapi.Model.Weather;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class WeatherData {

    @SerializedName("data")
    public List<Weather> data;

}
