package com.example.darkskyapi.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
@Entity
public class Weather {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "latitude")
    @SerializedName("latitude")
    public String latitude;

    @ColumnInfo(name = "language")
    @SerializedName("language")
    public String language;

    @ColumnInfo(name = "longitude")
    @SerializedName("longitude")
    public String longitude;

    @ColumnInfo(name = "summary")
    @SerializedName("summary")
    public String summary;

    @ColumnInfo(name = "temperatureMax")
    @SerializedName("temperatureMax")
    public Double temperature;

    @ColumnInfo(name = "humidity")
    @SerializedName("humidity")
    public Double humidity;

    @ColumnInfo(name = "pressure")
    @SerializedName("pressure")
    public Double pressure;

    @ColumnInfo(name = "windSpeed")
    @SerializedName("windSpeed")
    public Double windSpeed;

    @ColumnInfo(name = "precipIntensity")
    @SerializedName("precipIntensity")
    public Double precipIntensity;

    @ColumnInfo(name = "precipProbability")
    @SerializedName("precipProbability")
    public Double precipProbability;

    @ColumnInfo(name = "cloudCover")
    @SerializedName("cloudCover")
    public Double cloudCover;
    @ColumnInfo(name = "time")
    @SerializedName("time")
    public Double time;

    @ColumnInfo(name = "icon")
    @SerializedName("icon")
    public String icon;

    @ColumnInfo(name = "timezone")
    @SerializedName("timezone")
    public String timezone;

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
