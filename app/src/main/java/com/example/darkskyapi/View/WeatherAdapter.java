package com.example.darkskyapi.View;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.darkskyapi.Model.Weather;
import com.example.darkskyapi.Presenter.PresenterWeatherAdapter;
import com.example.darkskyapi.R;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IWeatherAdapter {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private PresenterWeatherAdapter presenter;
    private List<Weather> weathers;
    private ViewWeather viewWeather;

    public WeatherAdapter(ViewWeather viewWeather, List<Weather> weathers) {
        this.weathers = weathers;
        this.viewWeather = viewWeather;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_weather, parent, false);
        return new CustomViewHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof CustomViewHolder) {
            CustomViewHolder holder = (CustomViewHolder) viewHolder;
            final Weather weather = weathers.get(position);
            init(holder);
            setTextView(holder, weather);
            holder.layout.setTag(position);

        }
    }

    @Override
    public int getItemCount() {
        return (null != weathers ? weathers.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return weathers.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public void init(CustomViewHolder holder) {

        holder.tvTemperature.setVisibility(View.GONE);
        holder.tvHumidity.setVisibility(View.GONE);
        holder.tvPressure.setVisibility(View.GONE);
        holder.tvWindSpeed.setVisibility(View.GONE);
        holder.tvPrecipIntensity.setVisibility(View.GONE);
        holder.tvPrecipProbability.setVisibility(View.GONE);
        holder.tvCloudCover.setVisibility(View.GONE);
        holder.tvSummary.setVisibility(View.GONE);
        holder.tvDate.setVisibility(View.GONE);
    }

    @Override
    public void setTextView(WeatherAdapter.CustomViewHolder holder, Weather weather) {
        holder.tvTemperature.setText(presenter.getTemperatureText(weather.temperature));
        holder.tvTemperature.setVisibility(View.VISIBLE);

        holder.tvHumidity.setText(presenter.getHumidityText(weather.humidity));
        holder.tvHumidity.setVisibility(View.VISIBLE);

        holder.tvPressure.setText(presenter.getPressureText(weather.pressure));
        holder.tvPressure.setVisibility(View.VISIBLE);

        holder.tvWindSpeed.setText(presenter.getWindSpeedText(weather.windSpeed));

        holder.tvWindSpeed.setVisibility(View.VISIBLE);

        holder.tvPrecipIntensity.setText(presenter.getPrecipIntensityText(weather.precipIntensity));
        holder.tvPrecipIntensity.setVisibility(View.VISIBLE);

        holder.tvPrecipProbability.setText(presenter.getPrecipProbabilityText(weather.precipProbability));
        holder.tvPrecipProbability.setVisibility(View.VISIBLE);

        holder.tvCloudCover.setText(presenter.getCloudCoverText(weather.cloudCover));
        holder.tvCloudCover.setVisibility(View.VISIBLE);

        holder.tvSummary.setText(presenter.getSummaryText(weather.summary));
        holder.tvSummary.setVisibility(View.VISIBLE);

        holder.tvDate.setText(presenter.getDateText(weather.time));
        holder.tvDate.setVisibility(View.VISIBLE);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layout)
        protected View layout;

        @BindView(R.id.tvDate)
        protected TextView tvDate;

        @BindView(R.id.tvSummary)
        protected TextView tvSummary;

        @BindView(R.id.tvTemperature)
        protected TextView tvTemperature;

        @BindView(R.id.tvHumidity)
        protected TextView tvHumidity;

        @BindView(R.id.tvPressure)
        protected TextView tvPressure;

        @BindView(R.id.tvWindSpeed)
        protected TextView tvWindSpeed;

        @BindView(R.id.tvPrecipIntensity)
        protected TextView tvPrecipIntensity;

        @BindView(R.id.tvPrecipProbability)
        protected TextView tvPrecipProbability;

        @BindView(R.id.tvCloudCover)
        protected TextView tvCloudCover;

        public CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            presenter = new PresenterWeatherAdapter();
        }
    }

}
