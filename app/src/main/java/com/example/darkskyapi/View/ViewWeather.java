package com.example.darkskyapi.View;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.darkskyapi.Database.DatabaseClient;
import com.example.darkskyapi.Model.Weather;
import com.example.darkskyapi.Presenter.IPresenterWeather;
import com.example.darkskyapi.Presenter.PresenterWeather;
import com.example.darkskyapi.R;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewWeather extends Activity implements IViewWeather {
    String latitude;
    String longitude;
    String language;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.fabRefresh)
    FloatingActionButton fabRefresh;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.tvTimeZone)
    TextView tvTimeZone;

    @BindView(R.id.offlineView)
    View offlineView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private IPresenterWeather presenter;
    private LinearLayoutManager linearLayoutManager;
    private WeatherAdapter adapter;
    private List<Weather> weathers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_weather);
        ButterKnife.bind(this);
//        fabRefresh.setVisibility(View.GONE);
        fabRefresh.hide();
        progressBar.setVisibility(View.GONE);
        Bundle extras = getIntent().getExtras();
        offlineView.setVisibility(View.GONE);
        if (extras == null) {
            Log.d("isEmpty", "empty");
        } else {
            Log.d("isEmpty", "not empty");
            longitude = extras.getString("longitude");
            latitude = extras.getString("latitude");
            language = extras.getString("language");
        }
        init(longitude == null || latitude == null);
    }

    @OnClick(R.id.fab)
    public void onClickFab(View view) {
        startActivity(new Intent(ViewWeather.this, ViewDataInput.class));

    }

    @OnClick(R.id.fabRefresh)
    public void onClickFabRefresh(View view) {
        presenter.refreshDatabase();
        setVisibleBtnRefresh(false);

    }


    @Override
    public void init(boolean isEmpty) {

        tvTimeZone.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        weathers = new LinkedList<>();

        adapter = new WeatherAdapter(this, weathers);
        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(this)
                        .color(ContextCompat.getColor(this, R.color.grey_lines))
                        .build());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        if (isEmpty) {
            presenter = new PresenterWeather(this, weathers);

        } else {
            presenter = new PresenterWeather(this, latitude, longitude, language, weathers);
        }
    }

    @Override
    public void setWeather(List<Weather> weathers) {
        this.weathers = weathers;
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showAlert(String title, String message) {
        try {
            if (!isFinishing()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void getWeatherWithDatabase() {
        setVisible(true);
        class GetWeathers extends AsyncTask<Void, Void, List<Weather>> {

            @Override
            protected List<Weather> doInBackground(Void... voids) {
                List<Weather> weatherList = DatabaseClient
                        .getInstance(ViewWeather.this)
                        .getAppDatabase()
                        .weatherDao()
                        .getAll();
                return weatherList;
            }

            @Override
            protected void onPostExecute(List<Weather> weathers) {
                super.onPostExecute(weathers);
                presenter.loadWeatherWithDatabase(weathers);
            }
        }

        GetWeathers gw = new GetWeathers();
        gw.execute();
    }

    @Override
    public void saveWeatherToDatabase() {
        class SaveWeather extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                for (Weather weather : weathers) {
                    //adding to database
                    DatabaseClient.getInstance(ViewWeather.this).getAppDatabase()
                            .weatherDao()
                            .insert(weather);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }

        SaveWeather sw = new SaveWeather();
        sw.execute();
    }

    @Override
    public void deleteDatabase() {
        class DeleteWeathers extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(ViewWeather.this).getAppDatabase()
                        .weatherDao()
                        .deleteAll();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                saveWeatherToDatabase();
            }
        }
        DeleteWeathers deleteWeathers = new DeleteWeathers();
        deleteWeathers.execute();
    }
    @Override
    public void setVisible(boolean isVisible) {
        progressBar.setVisibility(isVisible ? View.VISIBLE : View.GONE);

    }

//    @SuppressLint("RestrictedApi")
    @Override
    public void setVisibleBtnRefresh(boolean isVisible) {
        if (isVisible) {
            fabRefresh.show();
        } else {
            fabRefresh.hide();
        }
    }

    @Override
    public void setTimeZone(String text) {
        tvTimeZone.setVisibility(View.VISIBLE);
        tvTimeZone.setText(text);
    }
}
