package com.example.darkskyapi.View;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.darkskyapi.Presenter.PresenterDataInput;
import com.example.darkskyapi.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewDataInput extends Activity implements IViewDataInput  {
    private PresenterDataInput presenter;
    private static final int LOCATION_REQUEST = 1;

    @BindView(R.id.edLatitude)
    EditText edLatitude;
    @BindView(R.id.edLongitude)
    EditText edLongitude;
    @BindView(R.id.btCheck)
    Button btCheck;
    @BindView(R.id.btnCategoryFilter)
    Button btnCategoryFilter;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.mylatlng)
    FloatingActionButton fabMylatlng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_data);
        ButterKnife.bind(this);
        presenter = new PresenterDataInput(this);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            Log.d("isEmpty", "empty");
            presenter.setNumberList(11);
        } else {
            Log.d("isEmpty", "not empty");
            String latitude = extras.getString("latitude");
            String longitude = extras.getString("longitude");
            String numberList = extras.getString("numberList");
            if(numberList!=null && !numberList.equals("")){
                if(presenter.checkIsNumeric(numberList)){
                    presenter.setNumberList(Integer.parseInt(numberList));
                }
            }else{
                presenter.setNumberList(11);
            }
            if (latitude != null && !latitude.equals("") && longitude != null && !longitude.equals("")) {
                setLatLng(latitude, longitude);
            }
        }
        setBtnCategoryFilter(presenter.getNumberList());
    }


    @Override
    public void showAlert(String title, String message) {
        try {
            if (!isFinishing()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void setBtnCategoryFilter(int which){
        btnCategoryFilter.setText(presenter.getLangToDialog(which));

    }
    @OnClick(R.id.btnCategoryFilter)
    public void onClickCategoryFilter(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select language");
        builder.setItems(presenter.createArray(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                setBtnCategoryFilter(which);

            }
        });
        builder.show();

    }

    @OnClick(R.id.btCheck)
    public void onClickCheck() {
        if (presenter.checkIsNumeric(edLatitude.getText().toString())) {
            if (presenter.checkIsNumeric(edLongitude.getText().toString())) {
                Intent intent = new Intent(ViewDataInput.this, ViewWeather.class);
                intent.putExtra("latitude", edLatitude.getText().toString());
                intent.putExtra("longitude", edLongitude.getText().toString());
                intent.putExtra("language", presenter.getLangToIntent());
                intent.putExtra("language", presenter.getLangToIntent());
                startActivity(intent);
            } else {
                showAlert("error", "Longitude is empty!");
            }
        } else {
            showAlert("error", "Latitude is empty!");
        }
    }

    @OnClick(R.id.fab)
    public void onClickFab() {
        Intent intent = new Intent(ViewDataInput.this, ViewMaps.class);
        intent.putExtra("numberList", "" + presenter.getNumberList());
        startActivity(intent);
    }

    @OnClick(R.id.mylatlng)
    public void onClickMylatlng() {
        askLocation();
    }

    @Override
    public void setLatLng(String latitude, String longitude) {
        edLatitude.setText(latitude);
        edLongitude.setText(longitude);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_REQUEST) {
            if (permissions.length == 1 &&
                    (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) ||
                            permissions[0].equals(Manifest.permission.ACCESS_COARSE_LOCATION))&&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("LOCATION_REQUEST", "yes");
                getLatLng();
            } else {
                Log.d("LOCATION_REQUEST", "no");
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void getLatLng() {
        LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Location bestLocation = null;
        if(mLocationManager==null) return;
        List<String> providers = mLocationManager.getProviders(true);
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() > bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        if(bestLocation!=null){

            setLatLng("" + bestLocation.getLatitude(), "" + bestLocation.getLongitude());
        }else{
            getLastLocationNewMethod();
        }
    }
    @Override
    public void getLastLocationNewMethod(){
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            setLatLng("" + location.getLatitude(), "" + location.getLongitude());
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    @Override
    public void askLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
        } else {
            getLatLng();
        }
    }
}
