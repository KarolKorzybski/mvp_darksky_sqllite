package com.example.darkskyapi.View;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.darkskyapi.Presenter.PresenterMaps;
import com.example.darkskyapi.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewMaps extends FragmentActivity implements OnMapReadyCallback, IViewMaps, GoogleMap.OnMarkerClickListener, ActivityCompat.OnRequestPermissionsResultCallback, GoogleMap.OnMyLocationClickListener {

    private static final int LOCATION_REQUEST = 1;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    private GoogleMap mMap;
    LatLng mLatLng;
    private Marker myMarker;
    private PresenterMaps presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_maps);
        ButterKnife.bind(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        showInfoDialog("Info","Hold the position you are interested in and click the map icon\nRemember!\nYou must turn on internet");
        presenter = new PresenterMaps();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String numberList = extras.getString("numberList");
            if(numberList!=null){
                if(presenter.checkIsNumeric(numberList)){
                    presenter.setNumberList(Integer.parseInt(numberList));
                }
            }
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                setMarker(latLng.latitude,latLng.longitude);
            }
        });
        askLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_REQUEST) {
            if (permissions.length == 1 &&
                    (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) ||
                            permissions[0].equals(Manifest.permission.ACCESS_COARSE_LOCATION))&&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("LOCATION_REQUEST", "yes");
                mMap.setMyLocationEnabled(true);
                mMap.setOnMyLocationClickListener(this);
            } else {
                Log.d("LOCATION_REQUEST", "no");
            }
        }
    }
    @OnClick(R.id.fab)
    public void onClickFab() {
        Intent intent = new Intent(ViewMaps.this, ViewDataInput.class);
        if (mLatLng != null) {
            intent.putExtra("latitude","" + mLatLng.latitude);
            intent.putExtra("longitude","" + mLatLng.longitude);
        }
        intent.putExtra("numberList", "" + presenter.getNumberList());
        startActivity(intent);
    }

    @Override
    public void showInfoDialog(String title, String message) {
        try {
            if (!isFinishing()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.equals(myMarker)) onClickFab();
        return false;
    }
    @Override
    public void askLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
        } else {
            mMap.setMyLocationEnabled(true);
            mMap.setOnMyLocationClickListener(this);
        }
    }

    @Override
    public void setMarker(double latitude, double longitude) {
        LatLng latLng = new LatLng(latitude,longitude);
        mMap.clear();
        myMarker = mMap.addMarker(new MarkerOptions().position(latLng).title("Position"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mLatLng = latLng;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        setMarker(location.getLatitude(),location.getLongitude());
    }
}
