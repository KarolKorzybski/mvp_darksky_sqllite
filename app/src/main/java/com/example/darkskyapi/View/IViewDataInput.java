package com.example.darkskyapi.View;


public interface IViewDataInput{
    void setLatLng(String latitude, String longitude);
    void showAlert(String title, String message);
    void getLatLng();
    void askLocation();
    void setBtnCategoryFilter(int which);
    void getLastLocationNewMethod();
}
