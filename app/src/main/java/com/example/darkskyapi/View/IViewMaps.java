package com.example.darkskyapi.View;

public interface IViewMaps {
    void showInfoDialog(String title, String message);
    void askLocation();
    void setMarker(double latitude, double longitude);

}
