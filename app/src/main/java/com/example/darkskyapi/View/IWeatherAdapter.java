package com.example.darkskyapi.View;


import com.example.darkskyapi.Model.Weather;

public interface IWeatherAdapter{
    void init(WeatherAdapter.CustomViewHolder holder);
    void setTextView(WeatherAdapter.CustomViewHolder holder, Weather weather);
}
