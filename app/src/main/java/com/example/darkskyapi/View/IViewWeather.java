package com.example.darkskyapi.View;


import com.example.darkskyapi.Model.Weather;

import java.util.List;


public interface IViewWeather{
    void init(boolean isEmpty);
    void setWeather(List<Weather> weathers);
    void setVisible(boolean isVisible);
    void setVisibleBtnRefresh(boolean isVisible);
    void setTimeZone(String text);
    void showAlert(String title, String message);
    void getWeatherWithDatabase();
    void saveWeatherToDatabase();
    void deleteDatabase();

}
